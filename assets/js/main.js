$(document).ready(function(e) {
    //var slideLength = ;
    var carousel=$('#owlCarousel');
    carousel.on('initialized.owl.carousel', function(event) {
        var items = event.item.count; 
        for(var i=0;i<items;i++){
            if(i===0){
                $('.dotsWrapper').append('<span class="active">'+parseInt(i+1)+'</span>');
            }else{
                $('.dotsWrapper').append('<span>'+parseInt(i+1)+'</span>');
            }
        }

        $('.dotsWrapper span').click(function(){
            var index = $(this).index();

            $('.dotsWrapper span.active').removeClass('active');
            $(this).addClass('active');

            carousel.trigger("to.owl.carousel",  [index, 500, true]);
        });
    });

    carousel.owlCarousel({
        nav:false,
        items:1,
        dots:false,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        dotsEach:Number,
        animateOut: 'fadeOut'
    });

    carousel.on('change.owl.carousel', function(event) {
        var item = event.item.index;

        $('.dotsWrapper span.active').removeClass('active');
        $('.dotsWrapper span').eq(item-1).addClass('active');
    });

    //Slider
    var myObject = {value:0};
    TweenMax.to(myObject, 2, {value:14, onUpdate:updateVal});

    function updateVal(){
        var prcnt = (myObject.value/15)*100;
        
        $('.slider').css('width',prcnt+'%');
        $('.slider .value').css('left',prcnt+'%');

        var val = Math.ceil(myObject.value);
        $('.slider .value').html('$'+val);
    }
    
});